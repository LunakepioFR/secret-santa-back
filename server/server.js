import router from './routes/routes.js';
import Db from './db/db.js';
import express from 'express';
import cors from 'cors';

const app = express()
const port = 8080

app.use(cors());
app.use(express.json());
app.use(router);

Db.sync()
.then((console.log("Connection to BDD")))
.catch(error => console.log(error))


app.listen(port, () => {
  console.log(`Example app listening on port ${port}`)
})