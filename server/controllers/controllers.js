import Person from "../models/persons.js";
import Santa from "../models/santa.js";
import personValidation from "../validation/personValidation.js";
import santaValidation from "../validation/santaValidation.js";

const getAll = (req, res) => {
  Person.findAll({
    attributes: {exclude :['createdAt', 'updatedAt']}
  })
  .then(persons => {
    res.status(200).json(persons)
  })
  .catch(error => res.status(500).json(error))
};

const getOne = (req, res) => {
  const {id} = req.params
  Person.findByPk(id)
  .then(person => {
    if(!person) return res.status(404).json({msg: 'Resources Not found'})
    res.status(200).json(person)
  })
  .catch((error) => res.status(500).json(error))
};

const getInfoMail = (req, res) => {
  const idSanta = body.idSanta;
  const idReceiver = body.idReceiver;
  console.log(body);
  Person.findByPk(idSanta)
  .then(person => {
    if(!person) return res.status(404).json({msg: 'Resources Not found'})
    Person.findByPk(idReceiver)
    .then(person => {
      if(!person) return res.status(404).json({msg: 'Resources Not found'})
      res.status(200).json(person.name)
    })
    .catch((error) => res.status(500).json(error))
    res.status(200).json(person.mail)
  })
  .catch((error) => res.status(500).json(error))
}

const createOne = (req, res) => {
  const {body} = req;
  const {error} = personValidation(body);
  if (error) return res.status(401).json(error.details[0].message);
  Person.create({...body})
  .then(()=>{ 
    res.status(201).json({msg: 'Created Resources'})
  })
  .catch(error => res.status(500).json(error))
};

const updateOne = (req, res) => {
  const {id} = req.params
  const {body} = req;

  Person.findByPk(id)
  .then(person =>{
    if(!person) return res.status(404).json({msg: 'Person not found!'})
    person.name = body.name;
    person.email = body.email;
    person.save()
    .then(() => res.status(201).json({msg: 'Updated Resources'}))
    .catch(error => res.status(500).json(error))
  })
  .catch(error => res.status(500).json(error))
}

const deleteOne = (req, res) => {
  const {id} = req.params
  Person.destroy({where : {id}})
  .then(ressource => {
    if (ressource === 0) return res.status(404).json({msg: 'Resource Not found!'})
    res.status(200).json({msg: "Deleted resources"})
  })
  .catch(error => res.status(500).json(error))
}

const addSanta = (req, res) => {
  const {body} = req;
  const {error} = santaValidation(body);
  if (error) return res.status(401).json(error.details[0].message);
  Santa.create({...body})
  .then(()=>{ 
    res.status(201).json({msg: 'Created Resources'})
  })
  .catch(error => res.status(500).json(error))
}

const deleteSanta = (req, res) => {
  const {id} = req.params
  Santa.destroy({where : {id}})
  .then(ressource => {
    if (ressource === 0) return res.status(404).json({msg: 'Resource Not found!'})
    res.status(200).json({msg: "Deleted resources"})
  })
  .catch(error => res.status(500).json(error))
}

const getAllSanta = (req, res) => {
  Santa.findAll({
    attributes: {exclude :['createdAt', 'updat    edAt']}
  })
  .then(santas => {
    res.status(200).json(santas)
  })
  .catch(error => res.status(500).json(error))
}

export {
  getAll,
  getOne,
  getInfoMail,
  createOne, 
  updateOne, 
  deleteOne,
  addSanta,
  deleteSanta,
  getAllSanta
}; 