import { Router } from "express";
import { getAll, getOne, getInfoMail, createOne, updateOne, deleteOne, addSanta, deleteSanta, getAllSanta } from "../controllers/controllers.js";

const router = Router();

// Person Routes
router.get('/getAll', getAll);
router.get('/getOne/:id', getOne);
router.get('/getInfoMail/:id', getInfoMail);
router.post('/createOne', createOne);
router.put('/updateOne/:id', updateOne);
router.delete('/deleteOne/:id', deleteOne)

// Santa routes
router.get('/getAllSanta', getAllSanta);
router.post('/addSanta', addSanta);
router.delete('/deleteSanta', deleteSanta);

export default router;