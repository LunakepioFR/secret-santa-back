import Joi from 'joi';

const personValidation = (body) => {
  const personSchema = Joi.object({
    name : Joi.string().min(3).max(40).trim().required(),
    mail : Joi.string().min(8).max(120).trim().required()
  })
  return personSchema.validate(body);
}

export default personValidation;