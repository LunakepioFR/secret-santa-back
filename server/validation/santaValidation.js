import Joi from 'joi';

const santaValidation = (body) => {
  const santaSchema = Joi.object({
    idSanta : Joi.number().required(),
    idReceiver : Joi.number().required()
  })
  return santaSchema.validate(body);
}

export default santaValidation;