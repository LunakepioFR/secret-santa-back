import sequelize from "sequelize";
import db from "../db/db.js";


const {DataTypes} = sequelize;
const person = db.define('person', {
  id : {
    type :  DataTypes.INTEGER.UNSIGNED,
    primaryKey: true,
    autoIncrement: true,
    allowNull: false
  },
  name : {
    type : DataTypes.STRING,
    allowNull: false,
  },
  mail : {
    type : DataTypes.STRING,
    allowNull: false,
  }}
)

export default person;