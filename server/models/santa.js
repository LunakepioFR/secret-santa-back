import sequelize from "sequelize";
import db from "../db/db.js";


const {DataTypes} = sequelize;
const santa = db.define('santa', {
  idSanta: {
    type :  DataTypes.INTEGER,
    allowNull: false,
    unique : true
  },
  idReceiver: {
    type :  DataTypes.INTEGER,
    allowNull: false,
    unique : true
  }
})

export default santa;