import { Sequelize } from "sequelize";

const options = {
    dialect: 'mysql', 
    host: 'mysql',
    define: {
        timestamps: false,
        freezeTableName: true,
    }
}

export default new Sequelize('secret_santa','QUENTIN','1234', options);